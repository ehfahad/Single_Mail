<?php

session_start();

include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\booklist\Book;
use App\Bitm\SEIP136104\Message\Message;

if(array_key_exists("itemPerPage",$_SESSION)) {
    if(array_key_exists("itemPerPage",$_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
}
else
    $_SESSION['itemPerPage']=5;

$itemPerPage=$_SESSION['itemPerPage'];

$obj =new Book();
$totalItem=$obj->count();
//echo $totalItem;
$totalPage=ceil($totalItem/$itemPerPage);

if(array_key_exists("pageNumber",$_GET))
    $pageNumber=$_GET['pageNumber'];
else
    $pageNumber=1;

$pagination= "";
for($count=1;$count<=$totalPage;$count++){
    $class=($pageNumber==$count)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom= $itemPerPage*($pageNumber-1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);

if(strtoupper($_SERVER['REQUEST_METHOD']=="GET")){
    $allInfo = $obj->paginator($pageStartFrom,$itemPerPage);
}

if(strtoupper($_SERVER['REQUEST_METHOD']=="POST")){
    $allInfo = $obj->prepare($_POST)->index();
}

if(strtoupper($_SERVER['REQUEST_METHOD']=="GET") && !empty($_GET['search'])){
    $allInfo = $obj->prepare($_GET)->index();
}

$availableTitle = $obj->getAllTitle();
$allTitle =  '"'.implode('","',$availableTitle).'"';

$availableDescription = $obj->getAllDescription();
$allDescription =  '"'.implode('","',$availableDescription).'"';

$availableSearchData = $obj->getAllSearchData();
$allSearchData =  '"'.implode('","',$availableSearchData).'"';
//Utility::dd($availableSearchData)

?>
<html>
<head>
    <title>Book Informations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">Atomic Project</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../../../index.php">Home</a></li>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->


<div class="container">
    <center><h2>Manage Booklist</h2></center>
    <?php if(array_key_exists("message",$_SESSION) && !empty($_SESSION['message'])): ?>
        <div id="message" class="alert alert-info">
            <center> <?php echo Message::message() ?></center>
        </div>
    <?php endif; ?>
    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trashlist</a>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select how many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option <?php if($itemPerPage==5): ?> selected <?php endif ?>>5</option>
                <option <?php if($itemPerPage==10): ?> selected <?php endif ?> >10</option>
                <option <?php if($itemPerPage==15): ?> selected <?php endif ?>>15</option>
                <option <?php if($itemPerPage==20): ?> selected <?php endif ?>>20</option>
                <option <?php if($itemPerPage==25): ?> selected <?php endif ?>>25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>


    <form class="form-inline" role="form" method="post" action="index.php">
        <div class="form-group">
            <label>Filter by Title:</label>
            <input type="text" class="form-control" name="filterByTitle" id="title">
        </div>
        <div class="form-group">
            <label>Filter by Description:</label>
            <input type="text" class="form-control" name="filterByDescription" id="description">
        </div>
        <button type="submit" class="btn btn-info">Filter</button>
    </form>

    <form action="index.php" method="get">
        <div class="form-group">
            <label>Search:</label>
            <input type="text" class="form-control" name="search" id="search">
        </div>
        <button type="submit">Search</button>
    </form>



    <table class="table">
        <thead>
        <tr>
            <td>SL</td>
            <td>ID</td>
            <td>Book Title</td>
            <td>Friend's Email</td>
            <td>Action</td>
        </tr>
        </thead>

        <tbody>
        <?php $sl=$pageStartFrom;
        foreach($allInfo as $info){ $sl++ ?>
            <tr class="success">
                <td><?php echo $sl ?></td>
                <td><?php echo $info->id?></td>
                <td><?php echo $info->title?></td>
                <td><?php echo $info->mail?></td>
                <td>
                    <a href="view.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">Update</a>
                    <a href="trash.php?id=<?php echo $info->id ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" onclick="return ConfirmDelete()" role="button">Delete</a>
                    <a href="mailOne.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Email Friend</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <?php if(strtoupper($_SERVER['REQUEST_METHOD']=="GET") && empty($_GET['search'])){ ?>
    <ul class="pagination">
        <?php if($pageNumber>1):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber-1 ?>'> Prev </a></li> <?php endif;?>
        <?php echo $pagination; ?>
        <?php if($pageNumber<$totalPage):?> <li><a href='index.php?pageNumber=<?php echo $pageNumber+1 ?>'> Next </a></li> <?php endif;?>
    </ul>

    <a href="pdf.php" class="btn btn-info" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-info" role="button">Download as XL</a>
    <a href="mail.php" class="btn btn-info" role="button">Email to Friend</a>
        
    <?php } ?>

</div>

<script>
    $('#message').show().delay(1500).fadeOut();

    function ConfirmDelete(){
        var x=confirm("Sure to delete?");
        if(x)
            return true;
        else
            return false;
    }
</script>

<script>
    $( function() {
        var availableTags = [
            <?php echo $allDescription ?>
        ];
        $( "#description" ).autocomplete({
            source: availableTags
        });
    } );
</script>

<script>
    $( function() {
        var availableTags = [
            <?php echo $allTitle ?>
        ];
        $( "#title" ).autocomplete({
            source: availableTags
        });
    } );
</script>

<script>
    $( function() {
        var availableTags = [
            <?php echo $allTitle ?>
        ];
        $( "#search" ).autocomplete({
            source: availableTags
        });
    } );
</script>

</body>
</html>