<?php

include_once ('../../../vendor/autoload.php');
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');

use App\Bitm\SEIP136104\booklist\Book;

$obj = new Book();
$allInfo = $obj->index();

$trs= "";
$sl=0;
foreach ($allInfo as $info): $sl++;

$trs.="<tr>";
$trs.="<td>$sl</td>  ";
$trs.="<td>$info->id   </td>";
$trs.="<td>$info->title   </td>";
$trs.="</tr>";

endforeach;

$html = <<<html
<table class="table">
        <thead>
        <tr>
            <td>SL</td>
            <td>ID</td>
            <td>Book Title</td>
        </tr>
        </thead>

        <tbody>
            $trs;
        </tbody>
    </table>
html;


$mpdf = new mPDF();

$mpdf->WriteHTML($html);

$mpdf->Output();


?>