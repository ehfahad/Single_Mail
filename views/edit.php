<?php
include_once ('../../../vendor\autoload.php');

use App\Bitm\SEIP136104\Utility\Utility;
use App\Bitm\SEIP136104\booklist\Book;


$obj= new Book();
$obj->prepare($_GET);
$singleItem = $obj->view()
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resources/Bootstrap/css/bootstrap.min.css">
    <script src="../../../Resources/Bootstrap/js/jquery.min.js"></script>
    <script src="../../../Resources/Bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Book Details</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" value="<?php echo $singleItem->id ?>">
            <input type="text" name="title" class="form-control"  placeholder="Enter Booktitle" value="<?php echo $singleItem->title?>">
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

</body>
</html>
